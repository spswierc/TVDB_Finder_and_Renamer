# TVDB_Finder_and_Renamer

Python application that fetches the names of all episodes from a television series given the show name using the TVDB api.

Also included is a batch file with several options to rename files within the same directory.
    1) Do you want to fetch a series?
        IF yes this will launch Finder.py to connect to TVDB and generate a new Episodes.txt
        IF no then it will attempt to use an already existing Episodes.txt
    2) What filetype would you like to use?
        5 options are given, 4 preset and option 5 allows the user to input custom filetype.
    3) Is this correct?
        Prints the before and after for the user to confirm
    4) Do you want to delete Episodes.txt?
        Gives the option to delete Episodes.txt or save it for later